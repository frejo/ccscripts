-- ComputerCraft program updater and installer

tArgs = { ... }
cmd = tArgs[1]
app = tArgs[2]
defaultSources = {"http://frejo.gitlab.io/ccscripts/scripts.list"}
sourcesListPath = "/files/apt/sources.list"
pgksListPath = "/files/apt/packages.list"

function install( pgk )
  packages = loadPackages()
  url = packages[pgk]
  if setContains(packages, pgk) then
    print( "Installing " .. pgk .. " with url " .. url )
    data = getUrl( url )
    -- Write data to file
    if fs.exists( pgk ) then
      print( "Error installing! File already exists. " )
      print( "Run 'apt upgrade <script>', to update the script, or remove it using 'apt remove <script>' before installing." )
    else
      file = fs.open( pgk, 'w' )
      file.write( data )
      file.close()
    end
  else
    print( "Unknown script '" .. pgk .. "'. If the script exists in one of the existing source lists, try running 'apt update'." )
  end
end

function upgrade( pgk )
  packages = loadPackages()
  if setContains( packages, pgk ) then
    uninstall( pgk )
    install( pgk )
  else
    print( "Unknown script '" .. pgk .. "'. If the script exists in one of the existing source lists, try running 'apt update'." )
  end
end

function uninstall( pgk )
  if fs.exists( pgk ) then
    print( "Uninstalling " .. pgk .. "." )
    shell.run( "rm " .. pgk)
    if fs.exists( pgk ) then
      print( "Error uninstalling!" )
      print( "Try running 'rm <script> manually. " )
    else
      print( "Done. " )
    end
  else
    print( "Skipping. Package not found." )
  end
end

function setContains( set, key )
  return set[key] ~= nil
end

function help()
  print( "Installs, uninstalls and updates scripts" )
  print( "Source lists are saved in /files/apt/sources.list" )
  print( "These lists points to tables with the names of and links to installable scripts" )
  print()
  print( "Usages: " )
  print( "apt install <script>    - installs script to this pc" )
  print( "apt remove <script>     - removes script from this pc" )
  --print( "apt purge <script>      - removes script and /files/<script> if found" )
  print( "apt upgrade <script>    - reinstalls script with newest version" )
  print( "apt source add <url>    - adds url to /files/apt/sources.list" )
  print( "apt source remove <url> - removes url from /files/apt/sources.list" )
  print( "apt source list         - lists urls in /files/apt/sources.list" )
  print( "apt list                - lists packages in /files/apt/packages.list" )
  print( "apt update              - updates the list of known apps" )
  print( "apt help                - displays this message" )
  print()
end

function getSources()
  if fs.exists( sourcesListPath ) then
    file = fs.open( sourcesListPath, 'r' )
    aSources = textutils.unserialize( file.readAll() )
    file.close()
  else
    file = fs.open( sourcesListPath, 'w' )
    file.write( textutils.serialize( defaultSources ) )
    file.close()
    aSources = defaultSources
  end
end

function getPgks()
  getSources()
  allPgks = {}
  for i = 1, #aSources do
    pageData = getUrl( aSources[i] )
    loadstring( pageData )()
    if scripts then
      for key,value in pairs( scripts ) do
        allPgks[key]= value
        --print( "Loaded script " .. key .. " with url " .. value .. "." )
      end
    end
  end
  for key,value in pairs( allPgks ) do
    print( "Loaded script " .. key .. " with url " .. value .. "." )
  end
  file = fs.open( pgksListPath, 'w' )
  file.write( textutils.serialize( allPgks ) )
  file.close()
end

function getUrl( url )
  encodedUrl = textutils.urlEncode( url )
  term.write( "Connecting to " .. url .. "... " )
  response = http.get( url )
  if response then
    print( "Success." )
    sResponse = response.readAll()
    response.close()
    -- print( "Recieved: " )
    -- print( sResponse )
    return sResponse
  else
    print( "Failed." )
  end
end

function loadPackages()
  if fs.exists( pgksListPath ) then
    file = fs.open( pgksListPath, 'r' )
    aPgks = textutils.unserialize( file.readAll() )
    file.close()
  else
    print( "Package list not found. Please run 'apt update'." )
  end  
  return aPgks
end

function changeSource()
  getSources()
  if tArgs[2] == "list" then
    print( "The following sources has been loaded: " )
    for i = 1, #aSources do
      print( " - " .. aSources[i] )
    end
    return
  end
  if tArgs[3] ~= nil then
    if tArgs[2] == "add" then
      print( "Adding list " .. tArgs[3] .. " to sources list." )
      table.insert(aSources, tArgs[3])
    elseif tArgs[2] == "remove" then
      print( "Removing list " .. tArgs[3] .. " from sources list." )
      local found = false
      for i = 1, #aSources do
        if aSources[i] == tArgs[3] then
          table.remove(aSources, i)
          found = true
          break
        end
      end
      if not found then
        print( "Source " .. tArgs[3] .. " not found" )
      end
    else
      print( "Wrong argument" )
      help()
      return
    end
    file = fs.open( sourcesListPath, 'w' )
    file.write( textutils.serialize( aSources ) )
    file.close()
  else
    print( "No source URL given" )
    help()
  end
end

function list()
  packages = loadPackages()
  print( "Available packages:")
  for key, value in pairs(packages) do
    print( " - " .. key )
  end
end

-- Runtime

if cmd == "install" then
  install( app )
elseif cmd == "remove" then
  uninstall( app )
elseif cmd == "upgrade" then
  upgrade( app )
elseif cmd == "update" then
  getPgks()
elseif cmd == "source" then
  changeSource()
elseif cmd == "list" then
  list()
elseif cmd == "help" then
  help()
else
  print("Unkown argument '" .. cmd .. "'.")
  help()
end