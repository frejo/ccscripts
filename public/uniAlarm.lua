rsPos = "left"
periPos = "left"
rednetSide = "right"
periType = peripheral.getType(periPos)
minPLevel = 60
alarmIsRunning = false
aStatPosX, aStatPosY = 10, 12
numColor = colors.orange
goodColor = colors.green
dangerColor = colors.red

rednetHostName = "tank"
rednetReceiverName = "MonitorPC"
rednetProtocol = "status"

local types = {
	["thermalexpansion:storage_tank"] = "tank",
	["railcraft:tank_iron_valve"] = "tank",
	["thermalexpansion:storage_cell"] = "powerRF",
}
local typeUnit = {
   tank = {
      "mB",
      --[1000] = "B",
      [1000000] = "kB",
      [1000000000] = "MB",
   },
   powerRF = {
      "RF",
      [1000] = "kRF",
      [1000000] = "MRF",
      [1000000000] = "GRF",
   }
}

history = {}
historyLenght = 10
hi = 1
lastLevel = 0

os.loadAPI("basic_lib")
 
function alarm(arg)
   if arg == "start" then
      redstone.setOutput(rsPos, true)
      alarmIsRunning = true
      term.setCursorPos(aStatPosX,aStatPosY)
      basic_lib.colorWrite(term, colors.red, "ALARM: LEVEL TOO LOW!!")
   elseif arg == "stop" then
      redstone.setOutput(rsPos, false)
      alarmIsRunning = false
      term.setCursorPos(aStatPosX,aStatPosY)
      term.clearLine()
   elseif arg == "getIsRunning" then
      return alarmIsRunning
   end
end

function makeAverage(level)
   if hi == historyLenght then
     hi = 1
   else
     hi = hi + 1
   end
   history[hi] = level - lastLevel
   lastLevel = level
   sum = 0
   count = 0
   for k,v in pairs(history) do
     sum = sum + v
     count = count + 1
   end
   average = sum/count
   return average
end

lastRednetMessage = ""

function sendStatusMessage(level, pLevel, delta, cUnit, dUnit)
   tMsg = {}
   tMsg["host"] = rednetHostName
   tMsg["content"] = level
   tMsg["percentage"] = pLevel
   tMsg["goodPercentage"] = minPLevel
   tMsg["drain"] = delta
   tMsg["contentUnit"] = cUnit
   tMsg["drainUnit"] = dUnit
   sMsg = textutils.serialise(tMsg)

   if sMsg == lastRednetMessage then
      return false
   else
      rednet.open(rednetSide)
      receiverID = rednet.lookup(rednetProtocol, rednetReceiverName)
      if receiverID then
         rednet.send(receiverID, sMsg, rednetProtocol)
      else
         rednet.broadcast(sMsg, rednetProtocol)
      end
      rednet.close()
      lastRednetMessage = sMsg
      return true
   end
end
 
term.clear()
term.setCursorPos(1,1)
 
if peripheral.isPresent(periPos) then
   print("Container is located on side: " .. periPos)
   print("with type: " .. periType)
   local peri = peripheral.wrap(periPos)
   local type = types[periType]
   local thisTypeUnit = typeUnit[type]
   rednetHostName = type
   if type == "tank" then
      maxLevel = peri.getTanks()[1]["capacity"]
   elseif type == "powerRF" then
      maxLevel = peri.getRFCapacity()
   else
      print("Error. Unconfigured type.")
	  exit()
   end
   convertedMax, convertedMaxUnit = basic_lib.addUnit(maxLevel, thisTypeUnit, 10)
   print()
   print("The capacity of the container is: " .. convertedMax .. " " .. convertedMaxUnit .. ".")
   print()
   
   cPosX, cPosY = term.getCursorPos()
   
   while true do
      term.setCursorPos(cPosX, cPosY)
      if type == "tank" then
         currentLevel = peri.getTanks()[1]["amount"]
      elseif type == "powerRF" then
         currentLevel = peri.getRFStored()
      else
         print("Error. Unconfigured type.")
         exit()
      end
      if not currentLevel then
         currentLevel = 0
      end
      pLevel = math.floor( currentLevel / maxLevel * 100 )
      term.clearLine()
      term.write("Current amount is: ")
      convertedLevel, convertedUnit = basic_lib.addUnit(currentLevel, thisTypeUnit)
      basic_lib.colorWrite(term, numColor, convertedLevel .. " " .. convertedUnit)
      term.write(", which is ")
      if pLevel < minPLevel then
         basic_lib.colorWrite(term, dangerColor, pLevel .. "%")
         term.write(" of full")
         if alarmIsRunning == false then
            alarm("start")
         end
      else
         basic_lib.colorWrite(term, goodColor, pLevel .. "%")
         term.write(" of full")
         if alarmIsRunning == true then
            alarm("stop")
         end
      end
      delta1 = currentLevel - lastLevel
      delta10 = makeAverage(currentLevel)

      term.setCursorPos(cPosX, cPosY + 1)
      term.clearLine()
      term.write("Level change: ")
	  
      d1col = basic_lib.colorNumber(delta1)
      d1con, d1unit = basic_lib.addUnit(delta1, thisTypeUnit)
      basic_lib.colorWrite(term, d1col, d1con)
      term.write(" " .. d1unit .. "/s (1s); ")
	  
      d10col = basic_lib.colorNumber(delta10)
      d10con,d10unit = basic_lib.addUnit(delta10, thisTypeUnit)
      basic_lib.colorWrite(term, d10col, d10con)
      term.write(" " .. d10unit .. "/s (" .. historyLenght .. "s)")
      
      sendStatusMessage(convertedLevel, pLevel, d10con, convertedUnit, d10unit)
      sleep(1)
   end
 
else
   print("Container not found. Make sure the configuration is correct.")
end
