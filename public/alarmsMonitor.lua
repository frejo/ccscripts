rednetSide = "bottom"
rednetHostName = "MonitorPC"
protocol = "status"

monitors = {
   peripheral.wrap("monitor_0");
   peripheral.wrap("monitor_1");
}

os.loadAPI("basic_lib")

local cX, dX = 3, 3
local Ys = {
   powerRF = { 
      c = 2,
      d = 3,
   },
   tank = { 
      c = 4,
      d = 5,
   },
}

local names = {
   powerRF = "P",
   tank  = "T",
}

-- for k,monitor in pairs(monitors) do
--   monitor.write("Hello " .. k)
-- end

function rednetInit()
   rednet.open(rednetSide)
   rednet.host(protocol, rednetHostName)
end

function monInit()
   for k,mon in pairs(monitors) do
   mon.clear()
      for longName,shortName in pairs(names) do
         mon.setCursorPos(1, Ys[longName].c)
         mon.write(shortName)
      end
   end
end

function receive()
   local sendId, msg = rednet.receive(protocol)
   local t = textutils.unserialise(msg)
   print(msg)
   return t["host"], t["content"], t["percentage"], t["goodPercentage"], t["drain"], t["contentUnit"], t["drainUnit"]
end

rednetInit()
monInit()
while true do
   local h,c,p,gP,d,cU,dU = receive()
   for k,mon in pairs(monitors) do
      local yPos = Ys[h]
      mon.setCursorPos(cX, yPos.c)
      -- mon.clearLine()
      local col = basic_lib.colorNumber(p, gP)
      basic_lib.colorWrite(mon, col, basic_lib.round(c, 1) .. " " .. cU .. " (" .. p .. "%)              ")
      mon.setCursorPos(dX, yPos.d)
      mon.clearLine()
      col = basic_lib.colorNumber(d)
      basic_lib.colorWrite(mon, col, basic_lib.round(d, 1) .. " " .. dU .. "/s")
   end
end
