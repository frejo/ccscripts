rsPos = "left"
tankPos = "right"
rednetSide = "left"
tankType = peripheral.getType(tankPos)
minPLevel = 90
alarmIsRunning = false
aStatPosX, aStatPosY = 10, 12
numColor = colors.orange
goodColor = colors.green
dangerColor = colors.red

rednetHostName = "tank"
rednetReceiverName = "MonitorPC"
rednetProtocol = "status"

history = {}
historyLenght = 10
hi = 1
lastLevel = 0
 
function colorWrite(col, str)
   origCol = term.getTextColor()
   term.setTextColor(col)
   term.write(str)
   term.setTextColor(origCol)
end
 
function alarm(arg)
   if arg == "start" then
      redstone.setOutput(rsPos, true)
      alarmIsRunning = true
      term.setCursorPos(aStatPosX,aStatPosY)
      colorWrite(colors.red, "ALARM: LOW FLUID LEVEL!!")
   elseif arg == "stop" then
      redstone.setOutput(rsPos, false)
      alarmIsRunning = false
      term.setCursorPos(aStatPosX,aStatPosY)
      term.clearLine()
   elseif arg == "getIsRunning" then
      return alarmIsRunning
   end
end

function makeAverage(level)
   if hi == historyLenght then
     hi = 1
   else
     hi = hi + 1
   end
   history[hi] = level - lastLevel
   lastLevel = level
   sum = 0
   count = 0
   for k,v in pairs(history) do
     sum = sum + v
     count = count + 1
   end
   average = sum/count
   return average
end

function colorNumber(num, seperator, higherColor, lowerColor)
   seperator = seperator or 0
   higherColor = higherColor or goodColor
   lowerColor = lowerColor or dangerColor
   if num >= seperator then
      colorWrite(higherColor, num)
   else
      colorWrite(lowerColor, num)
   end
end

lastRednetMessage = ""

function sendStatusMessage(level, delta)
   tMsg = {}
   tMsg["host"] = rednetHostName
   tMsg["content"] = level
   tMsg["drain"] = delta
   sMsg = textutils.serialise(tMsg)

   if not sMsg == lastRednetMessage then
      rednet.open(rednetSide)
      receiverID = rednet.lookup(rednetProtocol, rednetReceiverName)
      rednet.send(receiverID, sMsg, rednetProtocol)
      rednet.close()
      lastRednetMessage = sMsg
      return true
   else
      return false
   end
end
 
term.clear()
term.setCursorPos(1,1)
 
if peripheral.isPresent(tankPos) and peripheral.getType(tankPos) == tankType then
   print("Tank is located on side: " .. tankPos)
   print("with type: " .. tankType)
   tank = peripheral.wrap(tankPos)
   maxLevel = tank.getTanks()[1]["capacity"]
   print()
   print("The capacity of the tank is: " .. maxLevel .. " mB.")
   print()
   
   cPosX, cPosY = term.getCursorPos()
   
   while true do
      term.setCursorPos(cPosX, cPosY)
      currentLevel = tank.getTanks()[1]["amount"]
      if not currentLevel then
         currentLevel = 0
      end
      pLevel = math.floor( currentLevel / maxLevel * 100 )
      term.clearLine()
      term.write("Current amount is: ")
      colorWrite(numColor, currentLevel .. " mB")
      term.write(", which is ")
      if pLevel < minPLevel then
         colorWrite(dangerColor, pLevel .. "%")
         term.write(" of full")
         if alarmIsRunning == false then
            alarm("start")
         end
      else
         colorWrite(goodColor, pLevel .. "%")
         term.write(" of full")
         if alarmIsRunning == true then
            alarm("stop")
         end
      end
      delta1 = currentLevel - lastLevel
      delta10 = makeAverage(currentLevel)

      term.setCursorPos(cPosX, cPosY + 1)
      term.clearLine()
      term.write("Level change: ")
      colorNumber(delta1)
      term.write(" mB/s (1s); ")
      colorNumber(delta10)
      term.write(" mB/s (" .. historyLenght .. "s); ")
      sleep(1)
   end
 
else
   print("Tank not found. Make sure the configuration is correct.")
end
