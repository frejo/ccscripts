rsPos = "bottom"
cellPos = "back"
rednetSide = "left"
minPLevel = 90
alarmIsRunning = false
aStatPosX, aStatPosY = 10, 12
numColor = colors.orange
goodColor = colors.green
dangerColor = colors.red

rednetHostName = "power"
rednetReceiverName = "MonitorPC"
rednetProtocol = "status"
 
history = {}
historyLenght = 10
hi = 1
lastLevel = 0

function colorWrite(col, str)
   origCol = term.getTextColor()
   term.setTextColor(col)
   term.write(str)
   term.setTextColor(origCol)
end
 
function alarm(arg)
   if arg == "start" then
      redstone.setOutput(rsPos, true)
      alarmIsRunning = true
      term.setCursorPos(aStatPosX,aStatPosY)
      colorWrite(colors.red, "ALARM: LOW ENERGY LEVEL!!")
   elseif arg == "stop" then
      redstone.setOutput(rsPos, false)
      alarmIsRunning = false
      term.setCursorPos(aStatPosX,aStatPosY)
      term.clearLine()
   elseif arg == "getIsRunning" then
      return alarmIsRunning
   end
end

function makeAverage(level)
   if hi == historyLenght then
     hi = 1
   else
     hi = hi + 1
   end
   history[hi] = level - lastLevel
   lastLevel = level
   sum = 0
   count = 0
   for k,v in pairs(history) do
     sum = sum + v
     count = count + 1
   end
   average = sum/count
   return average
end

function colorNumber(num, seperator, higherColor, lowerColor)
   seperator = seperator or 0
   higherColor = higherColor or goodColor
   lowerColor = lowerColor or dangerColor
   if num >= seperator then
      colorWrite(higherColor, num)
   else
      colorWrite(lowerColor, num)
   end
end

lastRednetMessage = ""

function sendStatusMessage(level, delta)
   tMsg = {}
   tMsg["host"] = rednetHostName
   tMsg["content"] = level
   tMsg["drain"] = delta
   sMsg = textutils.serialise(tMsg)

   if not sMsg == lastRednetMessage then
      rednet.open(rednetSide)
      receiverID = rednet.lookup(rednetProtocol, rednetReceiverName)
      rednet.send(receiverID, sMsg, rednetProtocol)
      rednet.close()
      lastRednetMessage = sMsg
      return true
   else
      return false
   end
end
 
term.clear()
term.setCursorPos(1,1)
 
if peripheral.isPresent(cellPos) then
   print("Energy cell is located on side: " .. cellPos)
   cell = peripheral.wrap(cellPos)
   maxLevel = cell.getRFCapacity()
   print()
   print("The capacity of the cell is: " .. maxLevel .. " RF.")
   print()
   
   cPosX, cPosY = term.getCursorPos()
   
   while true do
      term.setCursorPos(cPosX, cPosY)
      currentLevel = cell.getRFStored()
      pLevel = math.floor( currentLevel / maxLevel * 100 )
      term.clearLine()
      term.write("Current level is: ")
      colorWrite(numColor, currentLevel .. " RF")
      term.write(", which is ")
      if pLevel < minPLevel then
         colorWrite(dangerColor, pLevel .. "%")
         term.write(" of full")
         if alarmIsRunning == false then
            alarm("start")
         end
      else
         colorWrite(goodColor, pLevel .. "%")
         term.write(" of full")
         if alarmIsRunning == true then
            alarm("stop")
         end
      end
      delta1 = currentLevel - lastLevel
      delta10 = makeAverage(currentLevel)

      term.setCursorPos(cPosX, cPosY + 1)
      term.clearLine()
      term.write("Level change: ")
      colorNumber(delta1)
      term.write(" RF/s (1s); ")
      colorNumber(delta10)
      term.write(" RF/s (" .. historyLenght .. "s); ")
      sendStatusMessage(currentLevel, delta10)
      sleep(1)
   end
 
else
   print("Energy cell not found. Make sure the configuration is correct.")
end
