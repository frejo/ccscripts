
function colorWrite(mon, col, str)
   local origCol = term.getTextColor()
   mon.setTextColor(col)
   mon.write(str)
   mon.setTextColor(origCol)
end

function colorNumber(num, seperator, higherColor, lowerColor)
   local num = tonumber(num) or -1
   local seperator = seperator or 0
   local lowerColor = lowerColor or colors.red
   local returnColor = higherColor or colors.green
   if num < seperator then
      returnColor = lowerColor
   end
   return returnColor
end

function round(num, digits)
   if digits and digits>0 then
      local mult = 10^digits
      return math.floor(num * mult + 0.5) / mult
   end
   return math.floor(num + 0.5)
end

function addUnit(number, unitList, significantDigits)
   local unitList = unitList or {
      "U",
      [1000] = "kU",
      [1000000] = "MU",
      [1000000000] = "GU",
   }
   local significantDigits = significantDigits or 2
   local newUnit = unitList[1]
   local newNumber = number
   for size, unit in pairs(unitList) do
      if number >= size or number <= 0-size then
         newUnit = unitList[size]
         newNumber = number/size
      end
   end
   return round(newNumber, significantDigits), newUnit
end

function alarm(arg, rs, xPos, yPos)
   if arg == "start" then
      redstone.setOutput(rs, true)
      alarmIsRunning = true
      term.setCursorPos(xPos,yPos)
      colorWrite(colors.red, term, "ALARM: LEVEL TOO LOW!!")
   elseif arg == "stop" then
      redstone.setOutput(rs, false)
      alarmIsRunning = false
      term.setCursorPos(xPos,yPos)
      term.clearLine()
   elseif arg == "getIsRunning" then
      return alarmIsRunning
   end
end
